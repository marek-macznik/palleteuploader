﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO.Ports;
using System.Management;
using System.Diagnostics;
using System.Threading;
using System.IO;
using System.Media;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Text;

namespace PalleteUploader
{
    public partial class Window : Form
    {
        private const string stlinkCLIPath = @"C:\Program Files (x86)\STMicroelectronics\STM32 ST-LINK Utility\ST-LINK Utility\ST-LINK_CLI.exe";
        private const string palletesPath = @"..\..\Palletes";
        private bool STLinkConnected = false;
        private int playbackNum = 1;

        private const string prodKey = "LOUTILPARFAISxxx";
        private string ucID = "0FA49D15321EACB1";

        private byte specialCounter = 0;
        private byte specialBorder = 0;

        private const byte START_FRAME = 49;
        private const byte END_FRAME = 50;

        enum COMMANDS : byte
        {
            CHECK_DEV_CONNECTED = 0xA0,
            DEV_SERIAL_NUM_GET = 0xAA,
            GET_ID = 0xB0,
            GET_CALIB_DATE = 0xCC,
            NEW_FILE_CREATE = 0xD0,
            DEV_PALLETE_REFRESH = 0xDD,
            PALLETE_UPLOAD = 0xE0,
            DEV_DISCONNECT = 0xF0,
            DEV_PALLETES_GET = 0x90,
            DEV_SERVICE_MENU_ENABLE = 0x69
        };
        
        public Window()
        {
            InitializeComponent();
            Scan_Serial_Ports();
            Scan_For_Palletes();
        }

        private void Window_Load(object sender, EventArgs e)
        {
            List<USBDeviceInfo> lst = Scan_For_STLINK();
            if( lst.Count != 0 )
            {
                pbST.Image = Properties.Resources.stmicro;
                STLinkConnected = true;
            }
            else
            {
                pbST.Image = Properties.Resources.stmicroGray;
                STLinkConnected = false;
            }
            btSoftware.Enabled = STLinkConnected;
            Random rand = new Random();
            specialBorder = (byte)rand.Next(3, 10);
            //specialTimer.Enabled = true;
            btSpecial.Visible = false;
            cbDevCon.Enabled = false;
        }

        private void Window_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (snd != null)
            {
                //pbLogo.Image = Properties.Resources.problem;
                playbackNum += 1;
                e.Cancel = true;
                return; 
            }
            else
            {
                e.Cancel = false;
            }
           
            if (serialPort != null)
            {
                if(serialPort.IsOpen)
                {
                    serialPort.Close();
                }
                serialPort.Dispose();
            }
            if(worker != null)
            {
                worker.Dispose();
            }
        }

        private void Scan_Serial_Ports()
        {
            foreach(string port in SerialPort.GetPortNames())
            {
                cbPortNames.Items.Add(port);
            }
            try
            {
                cbPortNames.SelectedIndex = 0;
            }
            catch (System.ArgumentOutOfRangeException e)
            {
                Console.WriteLine(e.ToString());
                DialogResult result = MessageBox.Show("Brak podłączonych urządzeń", "Uwaga!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                if( result == DialogResult.OK )
                {
                }
            }
        }

        private void Scan_For_Palletes()
        {
            List<string> palletes = new List<string>(Directory.GetFiles(palletesPath, "*.txt"));
            foreach (string file in palletes)
            {
                clbPalletes.Items.Add(file);
            }
        }

        private List<USBDeviceInfo> Scan_For_STLINK()
        {
            List<USBDeviceInfo> devices = new List<USBDeviceInfo>();
            ManagementObjectCollection collection;
            using (var type = new ManagementObjectSearcher(@"Select * From Win32_PnPEntity WHERE Description LIKE ""STMicroelectronics%"" "))
            {
                collection = type.Get();
            }
            try
            {
                foreach (var dev in collection)
                {
                    devices.Add(new USBDeviceInfo((string)dev.GetPropertyValue("DeviceID"), (string)dev.GetPropertyValue("PnpDeviceID"),
                        (string)dev.GetPropertyValue("Description")));
                }
                if(collection.Count == 0)
                {
                    DialogResult result = MessageBox.Show("Brak podłączonego programatora ST-LINK", "Uwaga!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    if (result == DialogResult.OK)
                    {
                    }
                }
            }
            catch (ManagementException e)
            {
                Console.WriteLine(e.ToString());
            }
            collection.Dispose();
            return devices;
        }

        byte btnConnectCounter = 0;
        private void btConnect_Click(object sender, EventArgs e)
        {
            btnConnectCounter++;
            if(btnConnectCounter == 1)
            {
                try
                {
                    serialPort = new SerialPort(cbPortNames.SelectedItem.ToString());
                    serialPort.Open();
                    btConnect.Text = "Zawier";
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Brak dostępnych portów", "Uwaga!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    btnConnectCounter = 0;
                }
            }
            else if(btnConnectCounter == 2)
            {
                if(serialPort.IsOpen)
                {
                    if(Device_Check_Connection())
                    {
                        byte[] payload = { 0 };
                        cbDevCon.Checked = false;
                        byte[] _toSend = FrameBuilder(COMMANDS.DEV_DISCONNECT, ref payload, 0);
                        serialPort.Write(_toSend, 0, _toSend.Length);
                    }
                    serialPort.Close();
                    btConnect.Text = "Łotwórz";
                    btnConnectCounter = 0;
                    if (serialPort != null)
                    {
                        serialPort.Dispose();
                    }
                }
            }
        }

        private void Upload_Hex(string hexName)
        {
            Process upload = new Process();
            try
            {
                string cmd = stlinkCLIPath;
                upload.StartInfo.FileName = cmd;
                upload.StartInfo.Arguments = "-c SWD -p " + hexName + " -Rst";
                upload.Start();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private void btSoftware_Click(object sender, EventArgs e)
        {
            OpenFileDialog file = new OpenFileDialog();
            file.Filter = "SOFT | *.hex";
            file.Title = "Wybier soft do wciepania";
            file.Multiselect = false;
            if(file.ShowDialog() == DialogResult.OK)
            {
                Thread uploader = new Thread(() => Upload_Hex(file.FileName));
                uploader.Start();
                uploader.Join();
            }
            file.Dispose();
        }
        
        BackgroundWorker worker;
        private void btSpecial_Click(object sender, EventArgs e)
        {
            if (worker == null)
            {
                worker = new BackgroundWorker();
                worker.DoWork += Worker_DoWork;
                worker.RunWorkerCompleted += Worker_RunWorkerCompleted;
                worker.WorkerReportsProgress = true;
                worker.WorkerSupportsCancellation = true;
            }
            try
            {
                worker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            btServiceSet.Enabled = false;
            btCheckAll.Enabled = false;
            btSoftware.Enabled = false;
            btConnect.Enabled = false;
            btUpload.Enabled = false;
            btSpecial.Visible = false;
        }

        private void Worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if(e.Error != null)
            {
                MessageBox.Show(e.Error.ToString(), "Błąd!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if(e.Cancelled)
            {
                if(snd != null)
                {
                    snd.Stop();
                }
            }
            else
            {
                if(snd != null)
                {
                    snd.Dispose();
                    snd = null;
                    //pbLogo.Image = Properties.Resources.logoLoutil;
                    if(STLinkConnected)
                    {
                        btSoftware.Enabled = true;
                    }
                    btServiceSet.Enabled = true;
                    btCheckAll.Enabled = true;
                    btConnect.Enabled = true;
                    btUpload.Enabled = true;
                    btSpecial.Visible = true;
                }
            }
        }
        SoundPlayer snd;
        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            if(snd == null)
            {
                snd = new SoundPlayer(Properties.Resources.surprise);
            }
            for(int i = 0; i < playbackNum; i++)
            {
                snd.PlaySync();
            }
            playbackNum = 1;
            worker.ReportProgress(100);
        }

        private List<string> Get_Checked_Pallets()
        {
            List<string> palletes = new List<string>();
            foreach(string pallete in clbPalletes.CheckedItems)
            {
                palletes.Add(pallete);
            }
            return palletes;
        }



        /* ------------------------------------------------------------------------------- */
        /*                         INTER DEVICE COMMUNICATIONS                             */
        /* ------------------------------------------------------------------------------- */

        private byte[] WaitForResponse(int timeout)
        {
            double waitTimeout = timeout + DateTime.Now.TimeOfDay.TotalMilliseconds;
            while(! (DateTime.Now.TimeOfDay.TotalMilliseconds >= waitTimeout) )
            {
                int dataLen = serialPort.BytesToRead;
                if ( dataLen > 0)
                {
                    byte[] received = new byte[dataLen];
                    serialPort.Read(received, 0, dataLen);
                    return received;
                }
                else
                {
                    Application.DoEvents();
                }
            }
            return new byte[1];
        }

        private string FrameDecoder(ref byte[] buffer)
        {
            string result = "";

            if (buffer[0] == START_FRAME && buffer[buffer.Length - 1] == END_FRAME)
            {
                byte command = buffer[1];
                byte Nbytes = buffer[2];
                UInt16 Nlines = (UInt16)(((buffer[3] << 8)) + buffer[4]);
                byte CRC = (byte)(command ^ Nbytes);
                byte[] payload = new byte[Nbytes];
                string response;
                Array.Copy(buffer, 5, payload, 0, Nbytes);
                switch (command)
                {
                   case (byte)COMMANDS.CHECK_DEV_CONNECTED:
                        response = Encoding.ASCII.GetString(payload);
                        result = "DevCon: " + response;
                        break;
                    case (byte)COMMANDS.GET_ID:
                        for(int i = 0; i < payload.Length; i++)
                        {
                            payload[i] &= 0x0F;
                        }
                        response = BitConverter.ToString(payload).Replace("-", "");
                        int margin = response.Length / 2;
                        for(int i = 0; i < margin; i++)
                        {
                            response = response.Remove(i, 1);
                        }
                        result = "ID: " + response;
                        break;
                    case (byte)COMMANDS.NEW_FILE_CREATE:
                        response = Encoding.ASCII.GetString(payload);
                        result = "FileCreated: " + response;
                        break;
                    case (byte)COMMANDS.PALLETE_UPLOAD:
                        response = Encoding.ASCII.GetString(payload);
                        result = "Uploading: " + response;
                        break;
                    case (byte)COMMANDS.DEV_PALLETE_REFRESH:
                        response = Encoding.ASCII.GetString(payload);
                        result = "Refresh: " + response;
                        break;
                    case (byte)COMMANDS.GET_CALIB_DATE:
                        response = Encoding.ASCII.GetString(payload);
                        result = "CalibDate: " + response;
                        break;
                    case (byte)COMMANDS.DEV_SERVICE_MENU_ENABLE:
                        response = Encoding.ASCII.GetString(payload);
                        result = "Service: " + response;
                        break;
                    case (byte)COMMANDS.DEV_SERIAL_NUM_GET:
                        result = "Serial: ";
                        foreach(byte b in payload)
                        {
                            result += b.ToString("X2");
                        }
                        break;
                    case (byte)COMMANDS.DEV_PALLETES_GET:
                        response = Encoding.ASCII.GetString(payload);
                        result = "P: " + response + ',' + Nlines.ToString();
                        break;
                    default:
                        MessageBox.Show("Nieznana komenda", "Jakiś losowy błąd, który nie powinien mieć miejsca", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        break;
                }

                if (CRC != buffer[buffer.Length-2])
                {
                    result = "";
                    throw new Exception("Invalid CRC");
                }
            }           
            return result;
        }

        private byte[] FrameBuilder(COMMANDS cmd, ref byte[] payload, UInt16 Nlines)
        {
            byte[] _toSend; List<byte> byteList = new List<byte>();
            byteList.Add(START_FRAME);
            byteList.Add((byte)cmd);
            byteList.Add((byte)payload.Length);
            byte[] _bytesNlines = BitConverter.GetBytes(Nlines);
            byteList.Add(_bytesNlines[1]);
            byteList.Add(_bytesNlines[0]);
            foreach ( byte b in payload )
            {
                byteList.Add(b);
            }
            byteList.Add((byte)((byte)cmd ^ (byte)payload.Length));
            byteList.Add(END_FRAME);
            _toSend = byteList.ToArray();
            return _toSend;
        }
        /* ------------------------------------------------------------------------------- */


        private List<string> Device_Get_Palletes()
        {
            string response = "P: ";
            List<string> palletes = new List<string>();
            if(serialPort.IsOpen)
            {
                byte[] _payload = { 0x00 };
                byte[] _toSend = FrameBuilder(COMMANDS.DEV_PALLETES_GET, ref _payload, 1);
                serialPort.Write(_toSend, 0, _toSend.Length);
                byte[] _response = WaitForResponse(1000);
                response = FrameDecoder(ref _response);
                if(!response.Equals("P: "))
                {
                    string[] pal_and_count = response.Replace("P: ", "").Split(',');
                    palletes.Add(pal_and_count[0]);
                    uint palNum = UInt32.Parse(pal_and_count[1]) - 1;
                    byte[] _myResp = Encoding.ASCII.GetBytes("OK");
                    byte[] _newSend = FrameBuilder(COMMANDS.DEV_PALLETES_GET, ref _myResp, 1);
                    serialPort.Write(_newSend, 0, _newSend.Length);
                    while (palNum > 0)
                    {
                        _response = WaitForResponse(1000);
                        response = FrameDecoder(ref _response);
                        pal_and_count = response.Replace("P: ", "").Split(',');
                        palletes.Add(pal_and_count[0]);
                        palNum--;
                        serialPort.Write(_newSend, 0, _newSend.Length);
                    }
                }
            }
            return palletes;
        }
        
        private string Device_Get_SerialNumber()
        {
            string response = "Serial: ";
            if(serialPort.IsOpen)
            {
                byte[] _payload = { 0x00 };
                byte[] _toSend = FrameBuilder(COMMANDS.DEV_SERIAL_NUM_GET, ref _payload, 1);
                serialPort.Write(_toSend, 0, _toSend.Length);
                byte[] _response = WaitForResponse(100);
                response = FrameDecoder(ref _response);
            }
            return response;
        }
        
        private string Device_Get_Date()
        {
            string response = "CalibDate: ";
            if (serialPort.IsOpen)
            {
                byte[] _payload = { 0x00 };
                byte[] _toSend = FrameBuilder(COMMANDS.GET_CALIB_DATE, ref _payload, 1);
                serialPort.Write(_toSend, 0, _toSend.Length);
                byte[] _response = WaitForResponse(1000);
                response = FrameDecoder(ref _response);
            }
            if (!response.Equals("CalibDate: "))
            {
                Regex regx = new Regex("(?:(?:[0-9]{4})-(?:[0-9]{2})-(?:[0-9]{2}))");
                Match m = regx.Match(response);
                response = m.ToString();
            }
            return response;
        }
        
        private bool Device_Check_Connection()
        {
            string connState = "DevCon: 0";
            byte[] payload = { 0 };
            byte[] _toSend = FrameBuilder(COMMANDS.CHECK_DEV_CONNECTED, ref payload, 0);
            serialPort.Write(_toSend, 0, _toSend.Length);
            byte[] response = WaitForResponse(100);
            if(response.Length > 1)
            {
                try
                {
                    connState = FrameDecoder(ref response);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Nieoczekiwany błąd komunikacji. AWARIA UCIEKAJ!", "ERRORRRRR!!!!!oneoenoeneoneONREONEOEN!1", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            
            if (connState.Equals("DevCon: 1"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private string Device_Get_ID()
        {
            string ID = "ID: ";
            byte[] payload = { 0 };
            byte[] _toSend = FrameBuilder(COMMANDS.GET_ID, ref payload, 0);
            serialPort.Write(_toSend, 0, _toSend.Length);
            byte[] response = WaitForResponse(100);
            if(response.Length > 1)
            {
                try
                {
                    ID = FrameDecoder(ref response);
                    Regex regx = new Regex("[0-9A-Fa-f]{3,}");
                    Match match = regx.Match(ID);
                    ID = match.ToString();
                }
                catch (Exception e)
                {
                    Debug.Write(e.ToString());
                }
            }
            if(ID.Equals("ID: "))
            {
                throw new Exception("Nie odczytano ID urządzenia");
            }
            return ID;
        }
        
        private bool Device_Pallete_Refresh()
        {
            string refresh = "Refresh: KO";
            byte[] _payload = { 0 };
            byte[] _toSend = FrameBuilder(COMMANDS.DEV_PALLETE_REFRESH, ref _payload, 1);
            if(serialPort.IsOpen)
            {
                serialPort.Write(_toSend, 0, _toSend.Length);
                byte[] response = WaitForResponse(60000);
                refresh = FrameDecoder(ref response);
            }
            if(refresh.Equals("Refresh: OK"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool Check_If_File_Created(ref byte[] palleteNameBytes, ref UInt16 NLines)
        {
            string created = "FileCreated: ";
            byte[] _toSend = FrameBuilder(COMMANDS.NEW_FILE_CREATE, ref palleteNameBytes, 1);
            if(serialPort.IsOpen)
            {
                serialPort.Write(_toSend, 0, _toSend.Length);
                byte[] response = WaitForResponse(2000);
                created = FrameDecoder(ref response);
            }
            if(created.Equals("FileCreated: OK"))
            {
                NLines--;
                return true;
            }
            else
            {
                return false;
            }
        }

        private void Send_Binary_Pallete(ref string fileName, ref UInt16 NLines, ref Aes aes, ref string palleteName)
        {
            UInt16 lineCounter = 1;
            UInt16 lineTotal = NLines;
            ICryptoTransform encryptor = aes.CreateEncryptor();
            var fs = new FileStream(fileName, FileMode.Open);
            byte[] dataChunk = new byte[16];
            byte[] _encLine = new byte[dataChunk.Length * 4];
            byte[] _enc = new byte[16];
            using (BinaryReader br = new BinaryReader(fs))
            {
                while(br.BaseStream.Position != br.BaseStream.Length)
                {
                    for(int i = 0; i < 4; i++)
                    {
                        dataChunk = br.ReadBytes(16);
                        _enc = encryptor.TransformFinalBlock(dataChunk, 0, 16);
                        Array.Copy(_enc, 0, _encLine, i * 16, 16);
                    }
                    byte[] _toSend = FrameBuilder(COMMANDS.PALLETE_UPLOAD, ref _encLine, NLines);
                    serialPort.Write(_toSend, 0, _toSend.Length);
                    byte[] response = WaitForResponse(1000);
                    string result = FrameDecoder(ref response);
                    UInt32 percentage = (UInt32)((lineCounter * 100) / (lineTotal));
                    if (labPallete.InvokeRequired)
                    {
                        string palName = palleteName;
                        labPallete.Invoke(new MethodInvoker(() =>
                        {
                            labPallete.Text = palName + " " + percentage.ToString() + "%";
                        }));
                    }
                    else
                    {
                        labPallete.Text = palleteName + " " + percentage.ToString() + "%";
                    }

                    if (progbPalleteUpload.InvokeRequired)
                    {
                        progbPalleteUpload.Invoke(new MethodInvoker(() =>
                        {
                            progbPalleteUpload.Value = (int)(percentage);
                        }));
                    }
                    else
                    {
                        progbPalleteUpload.Value = (int)(percentage);
                    }

                    if (result.Equals("Uploading: OK"))
                    {
                        NLines--;
                        lineCounter++;
                    }
                    else
                    {
                        throw new Exception("Nieoczekiwany błąd transmisji");
                    }
                }
            }
        }

        private void Send_Pallete(ref string[] fileContent, ref UInt16 NLines, ref Aes aes, ref string palleteName)
        {
            UInt16 lineCounter = 1;
            UInt16 lineTotal = NLines;
            ICryptoTransform encryptor = aes.CreateEncryptor();
            foreach (string line in fileContent)
            {
                byte[] _byteLine = Encoding.ASCII.GetBytes(line);
                byte[] first = new byte[16];
                byte[] second = new byte[16];
                first = encryptor.TransformFinalBlock(_byteLine, 0, 16);
                second = encryptor.TransformFinalBlock(_byteLine, 16, 16);
                Array.Copy(first, 0, _byteLine, 0, 16);
                Array.Copy(second, 0, _byteLine, 16, 16);
                byte[] _toSend = FrameBuilder(COMMANDS.PALLETE_UPLOAD, ref _byteLine, NLines);
                serialPort.Write(_toSend, 0, _toSend.Length);
                byte[] response = WaitForResponse(1000);
                string result = FrameDecoder(ref response);
                UInt32 percentage = (UInt32)((lineCounter * 100) / (lineTotal));
                if(labPallete.InvokeRequired)
                {
                    string palName = palleteName;
                    labPallete.Invoke(new MethodInvoker(() =>
                    {
                        labPallete.Text = palName + " " + percentage.ToString() + "%";
                    }));  
                }
                else
                {
                    labPallete.Text = palleteName + " " + percentage.ToString() + "%";
                }

                if(progbPalleteUpload.InvokeRequired)
                {
                    progbPalleteUpload.Invoke(new MethodInvoker(() =>
                    {
                        progbPalleteUpload.Value = (int)(percentage);
                    }));
                }
                else
                {
                    progbPalleteUpload.Value = (int)(percentage);
                }
                
                if(result.Equals("Uploading: OK"))
                {
                    NLines--;
                    lineCounter++;
                }
                else
                {
                    throw new Exception("Nieoczekiwany błąd transmisji");
                }
            }
        }

        private void Prepare_Pallete(string palleteFile)
        {
            byte[] keyBytes = Encoding.ASCII.GetBytes(ucID);
            for (int i = 0; i < keyBytes.Length; i++)
            {
                keyBytes[i] -= 0x30;
            }
            byte[] iv = Encoding.ASCII.GetBytes(prodKey);
            Aes aes = new AesCryptoServiceProvider();
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;
            aes.KeySize = 128;
            aes.BlockSize = 128;
            aes.Key = keyBytes;
            aes.IV = iv;

            if (File.Exists(palleteFile))
            {
                string[] content = File.ReadAllLines(palleteFile);
                UInt16 Nlines = Convert.ToUInt16(content.Length);
                Regex regex = new Regex("[A-Za-z]+[_]?[A-Za-z]+[_]?[A-Za-z]+[A-Za-z0-9]?.txt");
                Match match = regex.Match(palleteFile);
                string palleteName = match.ToString().Substring(0, match.ToString().Length-4);
                byte[] bytesPalleteName = Encoding.ASCII.GetBytes(palleteName);
                if(Check_If_File_Created(ref bytesPalleteName, ref Nlines))
                {
                    try
                    {
                        Send_Binary_Pallete(ref palleteFile, ref Nlines, ref aes, ref palleteName);
                        // Send_Pallete(ref content, ref Nlines, ref aes, ref palleteName);
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show("Plik nie został stworzony", "UWAGA!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                else
                {
                    throw new Exception("Błąd przy tworzeniu plików");
                }
            }
        }
               
        private void Process_Palletes(List<string> palletes)
        {
            try
            {
                ucID = Device_Get_ID();
                foreach (var pallete in palletes)
                {
                    Prepare_Pallete(pallete);
                    byte[] _response = WaitForResponse(10000);
                    string end = FrameDecoder(ref _response);
                    if(!end.Equals("Uploading: END"))
                    {
                        throw new Exception("Nie zamknięto poprzedniej palety");
                    }
                }
                progbPalleteUpload.Value = 0;
                labPallete.Refresh();
                labPallete.Text = "Device is refreshing files";
                if (Device_Pallete_Refresh())
                {
                    labPallete.Text = "Done";
                    //btSpecial.Visible = true;
                }
                
            }
            catch (Exception e)
            {
                MessageBox.Show("Prepare Pallete threw an Exception", "UWAGA", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }

        private void btUpload_Click(object sender, EventArgs e)
        {
            List<string> toSend = Get_Checked_Pallets();
            try
            {
                Process_Palletes(toSend);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Process Palletes() threw Exception", "UWAGA", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }

        private void specialTimer_Tick(object sender, EventArgs e)
        {
            if(specialCounter < specialBorder)
            {
                specialCounter++;
            }
            else
            {
                specialCounter = 0;
                btSpecial.Visible = false;
                specialTimer.Enabled = false;
            }
        }

        private void btnCheck_Click(object sender, EventArgs e)
        {
            if(serialPort.IsOpen)
            {
                if (Device_Check_Connection())
                {
                    cbDevCon.Checked = true;
                }
                else
                {
                    MessageBox.Show("Podłącz urządzenie!", "Uwaga!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    cbDevCon.Checked = false;
                }
            }
        }

        private byte checkAllCnt = 0;
        private void btCheckAll_Click(object sender, EventArgs e)
        {
            checkAllCnt++;
            if(checkAllCnt == 1) // check all
            {
                for (int i = 0; i < clbPalletes.Items.Count; i++)
                {
                    clbPalletes.SetItemChecked(i, true);
                    btCheckAll.Text = "Zawier ptaszki";
                }
            }
            else if(checkAllCnt == 2)
            {
                for (int i = 0; i < clbPalletes.Items.Count; i++)
                {
                    clbPalletes.SetItemChecked(i, false);
                    btCheckAll.Text = "Postew ptaszki";
                }
                checkAllCnt = 0;
            }
        }

        private void btService_Click(object sender, EventArgs e)
        {
            if(serialPort.IsOpen)
            {
                byte[] _payload = { 0x00 };
                byte[] _toSend = FrameBuilder(COMMANDS.DEV_SERVICE_MENU_ENABLE, ref _payload, 1);
                serialPort.Write(_toSend, 0, _toSend.Length);
                byte[] _response = WaitForResponse(100);
                string result = FrameDecoder(ref _response);
                cbService.Checked = result.Equals("Service: OK");
            }
        }

        private void btDate_Click(object sender, EventArgs e)
        {
            string date = Device_Get_Date();
            if(!date.Equals("CalibDate: "))
            {
                tbDate.Text = date;
            }
            else
            {
                tbDate.Text = "Err";
            }
        }

        private void btGetSN_Click(object sender, EventArgs e)
        {
            string sn = Device_Get_SerialNumber();
            if(sn.Equals("Serial: "))
            {
                tbSN.Text = "Err";
            }
            else
            {
                tbSN.Text = sn.Replace("Serial: ", "");
            }
        }

        private void btGetPalletes_Click(object sender, EventArgs e)
        {
            listPalletes.Items.Clear();
            List<string> palletes = Device_Get_Palletes();
            if(palletes.Count != 0)
            {
                foreach(string pallete in palletes)
                {
                    listPalletes.Items.Add(pallete);
                }
            }
        }
    }

    class USBDeviceInfo
    {
        public string DeviceID { get; private set; }
        public string PnpDeviceID { get; private set; }
        public string Description { get; private set; }

        public USBDeviceInfo(string devID, string pnpDevID, string desc)
        {
            this.DeviceID = devID;
            this.PnpDeviceID = pnpDevID;
            this.Description = desc;
        }
    }
}
