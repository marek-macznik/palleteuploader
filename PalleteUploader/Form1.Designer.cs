﻿namespace PalleteUploader
{
    partial class Window
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Window));
            this.cbPortNames = new System.Windows.Forms.ComboBox();
            this.serialPort = new System.IO.Ports.SerialPort(this.components);
            this.btConnect = new System.Windows.Forms.Button();
            this.btSoftware = new System.Windows.Forms.Button();
            this.clbPalletes = new System.Windows.Forms.CheckedListBox();
            this.btUpload = new System.Windows.Forms.Button();
            this.btSpecial = new System.Windows.Forms.Button();
            this.pbST = new System.Windows.Forms.PictureBox();
            this.progbPalleteUpload = new System.Windows.Forms.ProgressBar();
            this.labPallete = new System.Windows.Forms.Label();
            this.specialTimer = new System.Windows.Forms.Timer(this.components);
            this.cbDevCon = new System.Windows.Forms.CheckBox();
            this.btnCheck = new System.Windows.Forms.Button();
            this.btCheckAll = new System.Windows.Forms.Button();
            this.btServiceSet = new System.Windows.Forms.Button();
            this.cbService = new System.Windows.Forms.CheckBox();
            this.btDate = new System.Windows.Forms.Button();
            this.btGetSN = new System.Windows.Forms.Button();
            this.tbDate = new System.Windows.Forms.TextBox();
            this.tbSN = new System.Windows.Forms.TextBox();
            this.btGetPalletes = new System.Windows.Forms.Button();
            this.listPalletes = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbST)).BeginInit();
            this.SuspendLayout();
            // 
            // cbPortNames
            // 
            this.cbPortNames.FormattingEnabled = true;
            this.cbPortNames.Location = new System.Drawing.Point(11, 12);
            this.cbPortNames.Name = "cbPortNames";
            this.cbPortNames.Size = new System.Drawing.Size(156, 21);
            this.cbPortNames.TabIndex = 1;
            // 
            // serialPort
            // 
            this.serialPort.BaudRate = 921600;
            // 
            // btConnect
            // 
            this.btConnect.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btConnect.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btConnect.Location = new System.Drawing.Point(173, 12);
            this.btConnect.Name = "btConnect";
            this.btConnect.Size = new System.Drawing.Size(88, 21);
            this.btConnect.TabIndex = 2;
            this.btConnect.Text = "Łotwórz";
            this.btConnect.UseVisualStyleBackColor = false;
            this.btConnect.Click += new System.EventHandler(this.btConnect_Click);
            // 
            // btSoftware
            // 
            this.btSoftware.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btSoftware.FlatAppearance.BorderSize = 5;
            this.btSoftware.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btSoftware.Location = new System.Drawing.Point(186, 558);
            this.btSoftware.Name = "btSoftware";
            this.btSoftware.Size = new System.Drawing.Size(74, 39);
            this.btSoftware.TabIndex = 4;
            this.btSoftware.Text = "WGREJ SOFTA";
            this.btSoftware.UseVisualStyleBackColor = true;
            this.btSoftware.Click += new System.EventHandler(this.btSoftware_Click);
            // 
            // clbPalletes
            // 
            this.clbPalletes.CheckOnClick = true;
            this.clbPalletes.FormattingEnabled = true;
            this.clbPalletes.Location = new System.Drawing.Point(11, 272);
            this.clbPalletes.Name = "clbPalletes";
            this.clbPalletes.Size = new System.Drawing.Size(250, 184);
            this.clbPalletes.Sorted = true;
            this.clbPalletes.TabIndex = 5;
            // 
            // btUpload
            // 
            this.btUpload.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btUpload.Location = new System.Drawing.Point(11, 462);
            this.btUpload.Name = "btUpload";
            this.btUpload.Size = new System.Drawing.Size(249, 32);
            this.btUpload.TabIndex = 6;
            this.btUpload.Text = "WCIEPEJ PALETY";
            this.btUpload.UseVisualStyleBackColor = true;
            this.btUpload.Click += new System.EventHandler(this.btUpload_Click);
            // 
            // btSpecial
            // 
            this.btSpecial.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.btSpecial.Image = ((System.Drawing.Image)(resources.GetObject("btSpecial.Image")));
            this.btSpecial.Location = new System.Drawing.Point(12, 558);
            this.btSpecial.Name = "btSpecial";
            this.btSpecial.Size = new System.Drawing.Size(77, 39);
            this.btSpecial.TabIndex = 7;
            this.btSpecial.UseVisualStyleBackColor = true;
            this.btSpecial.Click += new System.EventHandler(this.btSpecial_Click);
            // 
            // pbST
            // 
            this.pbST.Image = global::PalleteUploader.Properties.Resources.stmicro;
            this.pbST.Location = new System.Drawing.Point(92, 558);
            this.pbST.Name = "pbST";
            this.pbST.Size = new System.Drawing.Size(89, 39);
            this.pbST.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbST.TabIndex = 3;
            this.pbST.TabStop = false;
            // 
            // progbPalleteUpload
            // 
            this.progbPalleteUpload.Location = new System.Drawing.Point(12, 500);
            this.progbPalleteUpload.MarqueeAnimationSpeed = 10;
            this.progbPalleteUpload.Name = "progbPalleteUpload";
            this.progbPalleteUpload.Size = new System.Drawing.Size(248, 23);
            this.progbPalleteUpload.Step = 1;
            this.progbPalleteUpload.TabIndex = 8;
            // 
            // labPallete
            // 
            this.labPallete.BackColor = System.Drawing.Color.Transparent;
            this.labPallete.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labPallete.Location = new System.Drawing.Point(11, 526);
            this.labPallete.Name = "labPallete";
            this.labPallete.Size = new System.Drawing.Size(250, 29);
            this.labPallete.TabIndex = 9;
            this.labPallete.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // specialTimer
            // 
            this.specialTimer.Interval = 1000;
            this.specialTimer.Tick += new System.EventHandler(this.specialTimer_Tick);
            // 
            // cbDevCon
            // 
            this.cbDevCon.AutoSize = true;
            this.cbDevCon.Location = new System.Drawing.Point(14, 44);
            this.cbDevCon.Name = "cbDevCon";
            this.cbDevCon.Size = new System.Drawing.Size(15, 14);
            this.cbDevCon.TabIndex = 10;
            this.cbDevCon.UseVisualStyleBackColor = true;
            // 
            // btnCheck
            // 
            this.btnCheck.Location = new System.Drawing.Point(32, 39);
            this.btnCheck.Name = "btnCheck";
            this.btnCheck.Size = new System.Drawing.Size(229, 23);
            this.btnCheck.TabIndex = 11;
            this.btnCheck.Text = "OBEJRZ CZY DZIAŁO";
            this.btnCheck.UseVisualStyleBackColor = true;
            this.btnCheck.Click += new System.EventHandler(this.btnCheck_Click);
            // 
            // btCheckAll
            // 
            this.btCheckAll.Location = new System.Drawing.Point(12, 243);
            this.btCheckAll.Name = "btCheckAll";
            this.btCheckAll.Size = new System.Drawing.Size(120, 23);
            this.btCheckAll.TabIndex = 12;
            this.btCheckAll.Text = "Postew ptaszki";
            this.btCheckAll.UseVisualStyleBackColor = true;
            this.btCheckAll.Click += new System.EventHandler(this.btCheckAll_Click);
            // 
            // btServiceSet
            // 
            this.btServiceSet.Location = new System.Drawing.Point(138, 243);
            this.btServiceSet.Name = "btServiceSet";
            this.btServiceSet.Size = new System.Drawing.Size(103, 23);
            this.btServiceSet.TabIndex = 12;
            this.btServiceSet.Text = "Wincyj opcyji";
            this.btServiceSet.UseVisualStyleBackColor = true;
            this.btServiceSet.Click += new System.EventHandler(this.btService_Click);
            // 
            // cbService
            // 
            this.cbService.AutoSize = true;
            this.cbService.Enabled = false;
            this.cbService.Location = new System.Drawing.Point(246, 248);
            this.cbService.Name = "cbService";
            this.cbService.Size = new System.Drawing.Size(15, 14);
            this.cbService.TabIndex = 13;
            this.cbService.UseVisualStyleBackColor = true;
            // 
            // btDate
            // 
            this.btDate.Location = new System.Drawing.Point(12, 187);
            this.btDate.Name = "btDate";
            this.btDate.Size = new System.Drawing.Size(75, 23);
            this.btDate.TabIndex = 14;
            this.btDate.Text = "Dej datę";
            this.btDate.UseVisualStyleBackColor = true;
            this.btDate.Click += new System.EventHandler(this.btDate_Click);
            // 
            // btGetSN
            // 
            this.btGetSN.Location = new System.Drawing.Point(12, 214);
            this.btGetSN.Name = "btGetSN";
            this.btGetSN.Size = new System.Drawing.Size(75, 23);
            this.btGetSN.TabIndex = 14;
            this.btGetSN.Text = "Dej numera";
            this.btGetSN.UseVisualStyleBackColor = true;
            this.btGetSN.Click += new System.EventHandler(this.btGetSN_Click);
            // 
            // tbDate
            // 
            this.tbDate.Enabled = false;
            this.tbDate.Location = new System.Drawing.Point(92, 189);
            this.tbDate.Name = "tbDate";
            this.tbDate.Size = new System.Drawing.Size(166, 20);
            this.tbDate.TabIndex = 15;
            // 
            // tbSN
            // 
            this.tbSN.Enabled = false;
            this.tbSN.Location = new System.Drawing.Point(92, 216);
            this.tbSN.Name = "tbSN";
            this.tbSN.Size = new System.Drawing.Size(166, 20);
            this.tbSN.TabIndex = 15;
            // 
            // btGetPalletes
            // 
            this.btGetPalletes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btGetPalletes.Location = new System.Drawing.Point(170, 68);
            this.btGetPalletes.Name = "btGetPalletes";
            this.btGetPalletes.Size = new System.Drawing.Size(88, 115);
            this.btGetPalletes.TabIndex = 16;
            this.btGetPalletes.Text = "Obejrz jakie mo palety";
            this.btGetPalletes.UseVisualStyleBackColor = true;
            this.btGetPalletes.Click += new System.EventHandler(this.btGetPalletes_Click);
            // 
            // listPalletes
            // 
            this.listPalletes.FormattingEnabled = true;
            this.listPalletes.Location = new System.Drawing.Point(11, 72);
            this.listPalletes.Name = "listPalletes";
            this.listPalletes.Size = new System.Drawing.Size(155, 108);
            this.listPalletes.TabIndex = 17;
            // 
            // Window
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ClientSize = new System.Drawing.Size(272, 608);
            this.Controls.Add(this.listPalletes);
            this.Controls.Add(this.btGetPalletes);
            this.Controls.Add(this.tbSN);
            this.Controls.Add(this.tbDate);
            this.Controls.Add(this.btGetSN);
            this.Controls.Add(this.btDate);
            this.Controls.Add(this.cbService);
            this.Controls.Add(this.btServiceSet);
            this.Controls.Add(this.btCheckAll);
            this.Controls.Add(this.btnCheck);
            this.Controls.Add(this.cbDevCon);
            this.Controls.Add(this.labPallete);
            this.Controls.Add(this.progbPalleteUpload);
            this.Controls.Add(this.btSpecial);
            this.Controls.Add(this.btUpload);
            this.Controls.Add(this.clbPalletes);
            this.Controls.Add(this.btSoftware);
            this.Controls.Add(this.pbST);
            this.Controls.Add(this.btConnect);
            this.Controls.Add(this.cbPortNames);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Window";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LTPF  Pallete Updater";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Window_FormClosing);
            this.Load += new System.EventHandler(this.Window_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbST)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox cbPortNames;
        private System.IO.Ports.SerialPort serialPort;
        private System.Windows.Forms.Button btConnect;
        private System.Windows.Forms.PictureBox pbST;
        private System.Windows.Forms.Button btSoftware;
        private System.Windows.Forms.CheckedListBox clbPalletes;
        private System.Windows.Forms.Button btUpload;
        private System.Windows.Forms.Button btSpecial;
        private System.Windows.Forms.ProgressBar progbPalleteUpload;
        private System.Windows.Forms.Label labPallete;
        private System.Windows.Forms.Timer specialTimer;
        private System.Windows.Forms.CheckBox cbDevCon;
        private System.Windows.Forms.Button btnCheck;
        private System.Windows.Forms.Button btCheckAll;
        private System.Windows.Forms.Button btServiceSet;
        private System.Windows.Forms.CheckBox cbService;
        private System.Windows.Forms.Button btDate;
        private System.Windows.Forms.Button btGetSN;
        private System.Windows.Forms.TextBox tbDate;
        private System.Windows.Forms.TextBox tbSN;
        private System.Windows.Forms.Button btGetPalletes;
        private System.Windows.Forms.ListBox listPalletes;
    }
}

